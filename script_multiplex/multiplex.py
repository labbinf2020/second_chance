#!/user/bin/python3

'''
Script to repeat randomized and non-randomized n times.
'''

import sys
import os
import class_arguments
import randomized
import fasta_parser
    	
if __name__ == "__main__":
    ARGS = class_arguments.ArgumentParser(sys.argv[1:])
    ARGS.PARSER.add_argument("-n", dest="number_repeat", type=int, help="Number of times to repeat"\
, required=True)
    ARGS.PARSER.add_argument("-r", dest="save_file", type=str, help="Name for output file", required=True)

#Vai adicionar o argumento para guardar ficheiro .fasta
    SAVE_FILE = ARGS.argument_handler().save_file
    SEED = ARGS.argument_handler().random_seed
    randomized.seed(SEED)

#Para introduzir o ficheiro
    FASTA_HANDLE = ARGS.argument_handler().input_file

#Para introduzir a percentagem
    PERCENTAGE = ARGS.argument_handler().desired_percentage

#Para introduzir a seed
    SEQUENCES = fasta_parser.fasta_dictionary(FASTA_HANDLE)

#Para repetir o output obtido n vezes para um determinado seed (caso dado)
    N_TIMES = ARGS.argument_handler().number_repeat
    
    for x in range(N_TIMES):
#Introduz a percentagem de missings ás sequências e grava-as num ficheiro fasta        
        ULTIMATE_DICTIONARY = randomized.inserting_missing_data(SEQUENCES, PERCENTAGE)
        FINAL_DICT = randomized.fasta_writer(ULTIMATE_DICTIONARY)
        with open(f"{SAVE_FILE}{x}.fasta", "w") as f:
            f.write(FINAL_DICT)
