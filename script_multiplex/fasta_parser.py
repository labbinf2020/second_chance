"""
Convert fasta_file(input) into a dictionary.
"""
def fasta_dictionary(fasta_handle):
    """
    Takes fasta_handle variable(fasta sequences) as an input and associates the fasta index as
    the dictionary keys and the sequences as their respective value and returns the dictionary.
    """
    dictionary = {}
    for line in fasta_handle:
        line = line.strip()
        if line.startswith(">"):
            taxa = line[1:]
            if taxa not in dictionary:
                dictionary[taxa] = ""
                continue
        dictionary[taxa] += line
    return dictionary
