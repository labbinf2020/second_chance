#!/user/bin/python3

"""
script to insert a desired % of missing data in a fasta file randomly.
"""

import sys
from random import randint, seed
import re
import class_arguments
import fasta_parser
import time

def inserting_missing_data(fasta_dict, percent):
    """
    Inserts missing values randomly from the inputed dictionary and returns it.
    """
    random_dictionary = {}
    for key, val in fasta_dict.items():
        dict_random = ["N" if randint(1, 100) <= percent else x for x in val]
        random_dictionary[key] = "".join(dict_random)
    return random_dictionary

def fasta_writer(fasta_dict):
    """
    transform from dict to fasta
    """
    result = ""
    for key, value in fasta_dict.items():
        value_64 = '\n'.join(re.findall('.{1,60}', value))
        n = ">" + key +"\n" + value_64 + "\n"
        result += n + "\n"
    return result

if __name__ == "__main__":
    ARGS = class_arguments.ArgumentParser(sys.argv[1:])
    FASTA_HANDLE = ARGS.argument_handler().input_file
    PERCENTAGE = ARGS.argument_handler().desired_percentage
    SEED = ARGS.argument_handler().random_seed
    seed(SEED)
    SEQUENCES = fasta_parser.fasta_dictionary(FASTA_HANDLE)
    ULTIMATE_DICTIONARY = inserting_missing_data(SEQUENCES, PERCENTAGE)
    DICT = fasta_writer(ULTIMATE_DICTIONARY)
    print(DICT)