# second_chance

# Multiplex

The multiplex.py run randomized script n times with a desire percentage and seed, finally save the output in n files.

## How to run

- To run it has to be in the repository where the scripts are located.

    `python3 multiplex.py`

- Required arguments

    `-f` input file name

    `-p` percentage

    `-n` number of times to repeat

    `-r` save the output file 

- If you need help

    `python3 multiplex.py -h`
    
Repositório para melhoria de notas "Laboratório de Bioinformática" 2020/2021
