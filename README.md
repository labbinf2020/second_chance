# second_chance

Repositório para melhoria de notas "Laboratório de Bioinformática" 2020/2021

## Dockerfile

Para correr o Dockerfile, basta correr o seguinte comando para criar a imagem:

`docker build -t [nome da imagem]:[tag] .`

Depois de criar a imagem, para a executar e criar o container basta:

`docker run -v /home/daniela/Documents/LABINF/second_chance/Second_Chance_Workflow/workflow/scripts/data -it [nome da imagem]:[tag] /bin/bash`
 
Para correr o snakemake, na shell do container:

`OUTPUTDIR="/scripts/data/D01/output" DATASET="/scripts/data/D01/input/D001a.json" snakemake Ete_tools_Any --cores all`

Contudo, ao executar o comando anterior vai ocorrer um erro por não conseguir correr as ete3 devido ao PyQt5. Para resolver, basta alterar "Ete_tools_Any" pela rule do snakemake que se pretende 
