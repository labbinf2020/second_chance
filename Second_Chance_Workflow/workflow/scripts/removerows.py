#!/usr/bin/env python
import sys

inFile = sys.argv[1]
outFile = sys.argv[2]
def pvalue(row):
    return float(row.split()[7]) > 0.05
open_file = open(inFile, 'r')
first_line= open_file.readline()
required_rows = [row for row in open_file if not pvalue(row)]
df = first_line + ''.join(required_rows)
with open(outFile, 'w') as o:
	for line in df:
		o.write(line)
