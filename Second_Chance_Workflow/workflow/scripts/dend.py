import dendropy
import sys

inFile = sys.argv[1]
outFile = sys.argv[2]
mle = dendropy.Tree.get(path=inFile, schema="nexus")
mle.write(path=outFile, schema="newick")
