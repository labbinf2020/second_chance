#!/bin/bash

sumtrees.py --min-clade-freq=0.5 --output-tree-filepath=$1 $2 $3
