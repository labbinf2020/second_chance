import sys 
import psycopg2

param_dict = {
    'host'        : 'localhost',
    'database'    : 'teste',
    'user'        : 'marcelo',
    'password'    : 'teste'
}


def insert_iqtree(conn, data, table_name, column_name, dataset_id):
    '''
    This function uses as input the svg file and iqtree table provided alongside a dataset id that the user states to input
    the data on the correct table
    '''
    cursor = conn.cursor()
    with open(sys.argv[1]) as data_file:
        data = "".join(data_file.readlines())
    query_ds = "SELECT * FROM {} WHERE dataset_id = '{}'".format(table_name, dataset_id)
    cursor.execute(query_ds)
    result = cursor.fetchall()
    try:
        if (result):
            query_text = "UPDATE {} SET {} = '{}' WHERE dataset_id = '{}'".format(table_name, column_name, data, dataset_id)
        else:
            query_text = "INSERT INTO {} (dataset_id, {}) VALUES ('{}', '{}')".format(table_name, column_name, dataset_id, data)
        cursor.execute(query_text)
        conn.commit()
        print('Table ' + table_name + ' updated successfully')
    except (Exception, psycopg2.DatabaseError) as error:
        print("Error: %s" % error)
        conn.rollback()


def connect(param_dict):
    '''
    Connects to the postgres DB
    '''
    conn = None
    try:
        print('Connecting to the PostgreSQL database...')
        conn = psycopg2.connect(**param_dict)
    except (Exception, psycopg2.DatabaseError) as error:
        print('Connection not successful')
        sys.exit(1)
    print('Connection Successful')
    return conn


if __name__ == "__main__":
    conn = connect(param_dict)
    with open(sys.argv[1]) as data_file:
        data = data_file.readlines()
    insert_iqtree(conn, data, sys.argv[2], sys.argv[4], sys.argv[3])
    conn.close()