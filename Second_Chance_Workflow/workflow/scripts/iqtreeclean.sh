#!/bin/bash
sed -n '/for trees with branch lengths./,/deltaL  :/p' $1 | sed '1,2d' | sed 's/--/ /g' | sed '$d' | sed '$d' | sed '2d' > $2
