
configfile: "config.json"
dataset= os.environ.get("DATASET", "10 20").split()
outputdir= os.environ.get("OUTPUTDIR", "10 20")
percentmissing = config["percentage"]
#print(percentmissing)
#command :  OUTPUTDIR="OUTPUT_PATH" DATASET="PATH/your_file.json" snakemake IqTree_Analysis_ML --cores all


                                                                #### Árvores a 100% de data ###
rule Get_Sequence_json:
    input:dataset
    output:outputdir +"/sequence.fasta"
    run:
    	 shell("python3 scripts/json_to_fasta.py -f {input} -d {outputdir} -s sequence")
    	 
rule Align_Sequences:
    input:outputdir + "/sequence.fasta"
    output:outputdir + "/sequence_Aligned.fasta"
    priority:5
    shell:"mafft {input} > {output}"


rule Create_MaximumLikelyhood_Trees:
    input: outputdir +"/sequence_Aligned.fasta"
    output: outputdir + "/Trees_Original/RAxML_bipartitions.MLTree"
    priority:4
    run:
        shell("{config[RaxMLHPC]} -x {config[rapidBootstrapRandomNumberSeed]} -f {config[algorithm]} -m {config[model]} -p {config[seed]} -N {config[number]} -T {config[threads]} -s {input} -n MLTree ")
        shell("mv -t {outputdir}/Trees_Original *.MLTree")


rule Convert_to_Nexus:
    input: outputdir +"/sequence_Aligned.fasta"
    output:outputdir +"/sequence_Aligned.nex"
    priority:3
    shell:"seqmagick convert --output-format nexus --alphabet dna {input} - | sed 's/missing=?/missing=n/' > {output}"


rule Add_MrBayes:
    input:outputdir +"/sequence_Aligned.nex"
    output:outputdir +"/sequence_Aligned_MB.nex"
    priority:2
    shell:"printf 'begin mrbayes;\n\tset autoclose=yes;\n\tmcmcp ngen={config[ngen]} printfreq={config[printfreq]} samplefreq={config[samplefreq]} diagnfreq={config[diagnfreq]} nchains={config[nchains]} savebrlens={config[savebrlens]} filename={config[filename]};\n\tmcmc;\n\tsumt filename={config[filename]};\nend;\n' | cat {input} - > {output} "


rule Create_Bayesian_Trees:
    input:outputdir +"/sequence_Aligned_MB.nex"
    output:outputdir +"/Trees_Original/BIrun.con.tre",outputdir +"/Trees_Original/BIrun.run1.t", outputdir +"/Trees_Original/BIrun.run2.t"
    priority:1
    run:
        shell("mb {input}")
        shell("mv -t {outputdir}/Trees_Original BIrun*")



                                                            ### Árvores para any %  of missing data ###

rule Randomize_Script_Any:
    input:outputdir +"/sequence.fasta"
    output:outputdir +"/sequence_Rand_"+percentmissing+".fasta"
    shell:"python3 scripts/randomized.py -f {input} -s 123 -p {config[percentage]}  > {output}"
    
##rule multiplex:
##    input:outputdir +"/sequence.fasta"
##    output:outputdir +"/sequence_Rand_"+percentmissing+".fasta"
##    run:"python3 scripts/multiplex.py -s sequence_rand -d {outputdir} -p {config[percentage]} -s 15 -f {input} > {output}"

rule Align_Rand_Sequences_Any:
    input:outputdir +"/sequence_Rand_"+percentmissing+".fasta"
    output:outputdir + "/sequence_Aligned_Rand_"+percentmissing+".fasta"
    priority:5
    shell:"mafft {input} > {output}"


rule Create_MaximumLikelyhood_Trees_For_Randomize_Any:
    input: outputdir +"/sequence_Aligned_Rand_"+percentmissing+".fasta"
    output: outputdir +"/Trees_"+percentmissing+"/RAxML_bipartitions.MLTreeRand"+percentmissing+""
    priority:4
    run:
        shell("{config[RaxMLHPC]} -x {config[rapidBootstrapRandomNumberSeed]} -f {config[algorithm]} -m {config[model]} -p {config[seed]} -N {config[number]} -T {config[threads]} -s {input} -n MLTreeRand{percentmissing} ")
        shell("mv -t {outputdir}/Trees_{percentmissing} *.MLTreeRand{percentmissing}")


rule Convert_to_Nexus_For_Randomize_Any:
    input:outputdir +"/sequence_Aligned_Rand_"+percentmissing+".fasta"
    output:outputdir +"/sequence_Aligned_Rand_"+percentmissing+".nex"
    priority:3
    shell:"seqmagick convert --output-format nexus --alphabet dna {input} - | sed 's/missing=?/missing=n/' > {output}"


rule Add_MrBayes_For_Randomize_Any:
    input:outputdir +"/sequence_Aligned_Rand_"+percentmissing+".nex"
    output:outputdir +"/sequence_Aligned_Rand_MB_"+percentmissing+".nex"
    priority:2
    shell:"printf 'begin mrbayes;\n\tset autoclose=yes;\n\tmcmcp ngen={config[ngen]} printfreq={config[printfreq]} samplefreq={config[samplefreq]} diagnfreq={config[diagnfreq]} nchains={config[nchains]} savebrlens={config[savebrlens]} filename={config[filenameAny]}{percentmissing};\n\tmcmc;\n\tsumt filename={config[filenameAny]}{percentmissing};\nend;\n' | cat {input} - > {output} "


rule Create_Bayesian_Trees_For_Randomize_Any:
    input:outputdir +"/sequence_Aligned_Rand_MB_"+percentmissing+".nex"
    output:outputdir +"/Trees_"+percentmissing+"/BIRandrun"+percentmissing+".con.tre",outputdir +"/Trees_"+percentmissing+"/BIRandrun"+percentmissing+".run1.t", outputdir +"/Trees_"+percentmissing+"/BIRandrun"+percentmissing+".run2.t"
    run:
        shell("mb {input}")
        shell("mv -t {outputdir}/Trees_{percentmissing} BIRandrun{percentmissing}*")




                                                    #### Análise com IQTree em árvores ML a Any% ###

rule IqTree_For_OriginalML:
    input:sequence=outputdir +"/sequence_Aligned.fasta",
          bipartition=outputdir +"/Trees_Original/RAxML_bipartitions.MLTree"
    output:outputdir +"/sequence_Aligned.fasta.treefile",
    run:
        shell("iqtree -s {input.sequence} -z {input.bipartition} -n 0 -zb 1000")


rule IqTree_For_Randomize_ML:
    input:sequence=outputdir +"/sequence_Aligned_Rand_"+percentmissing+".fasta",
          bipartition=outputdir +"/Trees_"+percentmissing+"/RAxML_bipartitions.MLTreeRand"+percentmissing+""
    output:outputdir +"/Iqtree_ML/" + percentmissing + "/sequence_Aligned_Rand_"+percentmissing+".fasta.treefile"
    run:
        shell("iqtree -s {input.sequence} -z {input.bipartition} -n {config[NumberIQTreeruns]} -zb 1000")
        shell("mv -t {outputdir}/Iqtree_ML/{percentmissing} {outputdir}/sequence_Aligned_Rand_{percentmissing}.fasta.*")


rule Connect_treefiles_ML:
    input:original=outputdir +"/sequence_Aligned.fasta.treefile",
          randomize=outputdir +"/Iqtree_ML/" + percentmissing + "/sequence_Aligned_Rand_"+percentmissing+".fasta.treefile"

    output:outputdir +"/Iqtree_ML/" + percentmissing + "/Iqtree_ML_"+percentmissing+".treels"
    run:
        shell("cat {input.original} {input.randomize} > {output}")


rule IqTree_Analysis_ML:
    input:sequence=outputdir +"/sequence_Aligned.fasta",
          treels=outputdir +"/Iqtree_ML/" + percentmissing + "/Iqtree_ML_"+percentmissing+".treels"
    output: outputdir +"/Iqtree_ML/" + percentmissing + "/sequence_Aligned.fasta.treefile",  outputdir+"/Iqtree_ML/"+percentmissing+"/sequence_Aligned.fasta.iqtree"
    run:
        shell("iqtree -s {input.sequence} -z {input.treels} -n 0 -zb 1000 -redo")
        shell("mv -t {outputdir}/Iqtree_ML/{percentmissing} {outputdir}/sequence_Aligned.fasta.* ")
 #       shell("mv -t {outputdir} {outputdir}/Iqtree_ML/{percentmissing}/sequence_Aligned.fasta.reduced ")
         
rule iqtree_table_ML:
    input:clean=outputdir+"/Iqtree_ML/"+percentmissing+"/sequence_Aligned.fasta.iqtree"
    output:outputdir +"/Iqtree_ML/"+percentmissing+"/Clean_sequence_Aligned.fasta.txt"
    run:
    	 shell("scripts/iqtreeclean.sh {input.clean} {output}")
    	 #shell("sed -n '/for trees with branch lengths./,/deltaL  :/p' {input.clean} | sed '1,2d' | sed 's/--/ /g' | sed '$d' | sed '$d' | sed '2d' > {output}")
    	 
rule summarize_ML:
    input:summ=outputdir+"/Iqtree_ML/"+percentmissing+"/Clean_sequence_Aligned.fasta.txt"
    output:outputdir +"/Iqtree_ML/"+percentmissing+"/Summarized_sequence_Aligned.fasta.txt"
    run:
         shell("python3 scripts/removerows.py {input.summ} {output}")



                                                    #### Análise com IQTree em árvores BI a any % missing data ###
                                                        	 
rule consensus_Mrbayes_to_Newick:
    input:run1=outputdir +"/Trees_Original/BIrun.run1.t",
          run2=outputdir +"/Trees_Original/BIrun.run2.t"
    output:outputdir +"/original.tre"
    run:
        shell("scripts/consensus.sh {output} {input.run1} {input.run2} ")
        
rule Mrbayes_to_Newick:
    input:outputdir + "/original.tre",
    output:outputdir +"/original.newick"
    run:
        shell("python3 scripts/dend.py {input} {output}")
    	 
rule IqTree_For_Original_BI:
    input:sequence=outputdir +"/sequence_Aligned_MB.nex",
          newick=outputdir +"/original.newick"
    output:outputdir +"/sequence_Aligned_MB.nex.treefile"
    shell: "iqtree -s {input.sequence} -z {input.newick} -n 0 -zb 1000"

rule Consensus_to_Randomize_Newick:
    input:run1=outputdir +"/Trees_"+percentmissing+"/BIRandrun"+percentmissing+".run1.t",
          run2=outputdir +"/Trees_"+percentmissing+"/BIRandrun"+percentmissing+".run2.t"
    output:outputdir + "/BIRandrun"+percentmissing+".tre"
    run:
        shell("scripts/consensus.sh {output} {input.run1} {input.run2} ")
#        shell("perl -pe 'BEGIN{ $/=\"&length\" } s/\[&length/\n/g' {input}  | sed 's/_mean.*}]//' > {output}")

rule Mrbayes_to_Randomize_Newick:
    input:outputdir +"/BIRandrun"+percentmissing+".tre"
    output:outputdir +"/Rand"+percentmissing+".newick"
    run:
        shell("python3 scripts/dend.py {input} {output} ")

rule IqTree_For_Randomize_BI:
    input:sequence=outputdir +"/sequence_Aligned_Rand_MB_"+percentmissing+".nex",
          newick=outputdir +"/Rand"+percentmissing+".newick"
    output:outputdir +"/Iqtree_BI/"+percentmissing+"/sequence_Aligned_Rand_MB_"+percentmissing+".nex.treefile"
    run:
        shell("iqtree -s {input.sequence} -z {input.newick} -n {config[NumberIQTreeruns]} -zb 1000")
        shell("mv -t {outputdir}/Iqtree_BI/{percentmissing} {outputdir}/sequence_Aligned_Rand_MB_{percentmissing}.nex.*")

rule Connect_treefiles_BI:
    input:original=outputdir +"/sequence_Aligned_MB.nex.treefile",
          multiplex=outputdir +"/Iqtree_BI/"+percentmissing+"/sequence_Aligned_Rand_MB_"+percentmissing+".nex.treefile"
    output:outputdir +"/Iqtree_BI_"+percentmissing+".treels"
    shell: "cat {input.original} {input.multiplex} > {output}"
    	 
rule IqTree_Analysis_BI:
    input:sequence=outputdir +"/sequence_Aligned_MB.nex",
          treels=outputdir +"/Iqtree_BI_"+percentmissing+".treels"
    output:outputdir +"/Iqtree_BI/"+percentmissing+"/sequence_Aligned_MB.nex.ckp.gz", outputdir+"/Iqtree_BI/"+percentmissing+"/sequence_Aligned_MB.nex.treefile", outputdir+"/Iqtree_BI/"+percentmissing+"/sequence_Aligned_MB.nex.iqtree"
    run:
         shell("iqtree -s {input.sequence} -z {input.treels} -n 0 -zb 1000 -redo")
         shell("mv -t {outputdir}/Iqtree_BI/{percentmissing} {outputdir}/sequence_Aligned_MB.nex.*")
         shell("mv -t {outputdir}/Iqtree_BI/{percentmissing} {outputdir}/Iqtree_BI_"+percentmissing+".treels")
         
         
rule iqtree_table_BI:
    input:clean=outputdir+"/Iqtree_BI/"+percentmissing+"/sequence_Aligned_MB.nex.iqtree"
    output:outputdir +"/Iqtree_BI/"+percentmissing+"/Clean_sequence_Aligned_MB.nex.txt"
    run:
    	 shell("scripts/iqtreeclean.sh {input.clean} {output}")

    	 
rule summarize_BI:
    input:summ=outputdir+"/Iqtree_BI/"+percentmissing+"/Clean_sequence_Aligned_MB.nex.txt"
    output:outputdir +"/Iqtree_BI/"+percentmissing+"/Summarized_sequence_Aligned_MB.nex.txt"
    run:
         shell("python3 scripts/removerows.py {input.summ} {output}")


                                                    #### ETE Tools ###

rule Ete_tools:
    input: fasta = outputdir +"/sequence_Aligned.fasta",
           bipartitions = outputdir + "/Trees_Original/RAxML_bipartitions.MLTree",
           treefile = outputdir+"/Iqtree_BI/"+percentmissing+"/sequence_Aligned_MB.nex.treefile"
    output: BI= outputdir +"/ete_tools/BI.svg",
            ML= outputdir +"/ete_tools/ML.svg"
    shell:("python3 scripts/etetools.py -f {input.fasta} -j {input.bipartitions} -k {input.treefile} -b {output.BI} -m {output.ML}")


rule Ete_tools_Any:
    input: fasta = outputdir +"/sequence_Aligned_Rand_"+percentmissing+".fasta",
           bipartitions = outputdir + "/Trees_"+percentmissing+"/RAxML_bipartitions.MLTreeRand"+percentmissing+"",
           treefile = outputdir+"/Iqtree_BI/"+percentmissing+"/sequence_Aligned_Rand_MB_"+percentmissing+".nex.treefile"
    output: BI= outputdir +"/ete_tools/BI_"+percentmissing+".svg",
            ML= outputdir +"/ete_tools/ML_"+percentmissing+".svg"
    shell:("python3 scripts/etetools.py -f {input.fasta} -j {input.bipartitions} -k {input.treefile} -b {output.BI} -m {output.ML}")
