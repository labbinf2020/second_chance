				# Download base image snakemake

FROM snakemake/snakemake:latest

				# Install raxml, mrbayes, dendropy...

RUN mkdir -p /scripts/

WORKDIR /scripts/

RUN pwd

RUN apt-get update && apt-get  install -y \
	python3 \
	python3-pip \
	&& pip3 install seqmagick \
	&& pip3 install perl \
	&& pip3 install dendropy \
	&& pip3 install ete3

RUN conda install -c bioconda iqtree

RUN apt-get install -y mrbayes mafft raxml

VOLUME second_chance/datasets/D001/ \
        second_chance/datasets/D002/ \
        second_chance/datasets/D003/ \
        second_chance/datasets/D004/ /scripts/

				# Copy the pipeline of snakemake

COPY Second_Chance_Workflow/workflow/scripts/class_arguments.py \
	Second_Chance_Workflow/workflow/scripts/dend.py \
	Second_Chance_Workflow/workflow/scripts/etetools.py \
	Second_Chance_Workflow/workflow/scripts/fasta_parser.py \
	Second_Chance_Workflow/workflow/scripts/Input_db_scripts_fasta_parser.py \
	Second_Chance_Workflow/workflow/scripts/Input_db_scripts_Input_fasta_json.py \
	Second_Chance_Workflow/workflow/scripts/Input_db_scripts_Input_fasta_random.py \
	Second_Chance_Workflow/workflow/scripts/Input_db_scripts_Input_svg_iqtree.py \
	Second_Chance_Workflow/workflow/scripts/Input_db_scripts_insert_on_table_on_column.py \
	Second_Chance_Workflow/workflow/scripts/Input_db_scripts_svg.py \
	Second_Chance_Workflow/workflow/scripts/randomized.py \
	Second_Chance_Workflow/workflow/scripts/randomized_and_non_randomized_multiplex.py \
	Second_Chance_Workflow/workflow/scripts/removerows.py \
	Second_Chance_Workflow/workflow/scripts/multiplex.py \
	Second_Chance_Workflow/workflow/scripts/Tree_comparison_Mrbayes2newick.sh \
	Second_Chance_Workflow/workflow/scripts/iqtreeclean.sh \
	Second_Chance_Workflow/workflow/scripts/consensus.sh \
	Second_Chance_Workflow/workflow/scripts/json_to_fasta.py /scripts/

COPY Second_Chance_Workflow/workflow/snakefile /scripts/
COPY Second_Chance_Workflow/workflow/config.json /scripts/

			# Add the pipeline scripts to the $PATH and
				# make sure they are executable

ENV PATH $PATH:/scripts/

RUN chmod +x /scripts/*.py

			# Define the command to execute when running
					# the container

CMD ["/bin/bash"]
