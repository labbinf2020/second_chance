# second_chance

# JSON to FASTA

This script will get the accession numbers from JSON file, then with the accession numbers search and retrieve FASTA sequences using the NBCI Entrez API.

## How to run

- The following script requires internet connection and the following packages: python3.6 or above , ncbi-entrez-direct for it to work.
- Please install it before using.
- To run it has to be in the repository where the scripts are located.

    `python3 json_to_fasta.py`

- Required arguments

    `-f` input file name

    `-s` output file name (save)
    
    `-d` file destination (path)

- If you need help

    `python3 json_to_fasta.py -h`
    
Repositório para melhoria de notas "Laboratório de Bioinformática" 2020/2021
