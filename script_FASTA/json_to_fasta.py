#!/usr/bin/env python3

#Import the libraries
import argparse 
import json
from Bio import Entrez
from Bio import SeqIO
import os

#A little description by choosing -h
parser = argparse.ArgumentParser(description = "This script will search and retrieve FASTA sequences using the NBCI Entrez API.")

#The input json file
parser.add_argument("-f", type=argparse.FileType(), dest="input_file", required=True, help="Select the input file")

#The name to save the output fasta sequences
parser.add_argument("-s", type=str, dest="save_file", help="Save the output file")

#The diretory destination (path) of the file
parser.add_argument("-d", dest="destination", required=True, help="The file destination")

ARGUMENTS = parser.parse_args()

INPUT_FILE = ARGUMENTS.input_file
OUTPUT_FILE = ARGUMENTS.save_file
OUTPUT_PATH = ARGUMENTS.destination

#Loads the json file
if INPUT_FILE:
	f = json.load(INPUT_FILE)
	
#Accession numbers from that json file
accession_number = f['accession_number']
print(f['accession_number'])

#Identification to NCBI know who you are
Entrez.email = "201800122@estudantes.ips.pt"

if not os.path.isfile(OUTPUT_FILE):
	#Path file and his name. Used NCBI tool "efetch" to download the fasta sequences from the accession numbers.
	complete_name = os.path.join(OUTPUT_PATH , OUTPUT_FILE + ".fasta")
	handle = Entrez.efetch(db="nucleotide", id=accession_number, rettype="fasta", retmode="text")
	create_file = open(complete_name, "w")
	create_file.write(handle.read())
	create_file.close()
	handle.close()

SeqIO.parse(complete_name, "fasta")
